package main

import (
	"gitlab.com/akita/akita"
)

type PingRsp struct {
	akita.MsgMeta

	SeqID int
}

func (p *PingRsp) Meta() *akita.MsgMeta {
	return &p.MsgMeta
}
