package main

import "gitlab.com/akita/akita"

type RspPingEvent struct {
	*akita.EventBase
	pingMsg *PingMsg
}
