package main

import (
	"flag"

	"gitlab.com/akita/mgpusim/benchmarks/heteromark/fir"
	"gitlab.com/akita/mgpusim/samples/runner"
)

var numData = flag.Int("length", 4096, "The number of samples to filter.")

func main() {
	flag.Parse()

	runner := new(runner.Runner).ParseFlag().Init()

	// FIR benchmark documentation
	// https://pkg.go.dev/gitlab.com/akita/mgpusim/benchmarks/heteromark/fir?tab=doc#NewBenchmark
	// hint: runner has the GPU driver
	benchmark := fir.NewBenchmark(________________)
	benchmark.Length = *numData

	// Runner Documentation
	// https://pkg.go.dev/gitlab.com/akita/mgpusim/samples/runner?tab=doc
	runner.AddBenchmark(______________)

	runner.Run()
}
