module gitlab.com/syifan/hpca2020_tutorial_handson

go 1.13

require (
	github.com/tebeka/atexit v0.1.0
	gitlab.com/akita/akita v1.10.1
	gitlab.com/akita/mem v1.8.0
	gitlab.com/akita/mgpusim v1.7.1
	gitlab.com/akita/noc v1.3.3
	gitlab.com/akita/util v0.3.0
)
